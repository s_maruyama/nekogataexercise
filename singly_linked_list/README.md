# SinglyLinkedList の実装

以下の要件を満たす SinglyLinkedList を実装する。

* 空のリスト は `SinglyLinkedList::Null` のインスタンスで表現する
  * `SinglyLinkedList::Null` は、 `SinglyLinkedList` のサブクラスである
* `SinglyLinkedList` は、最初の要素への参照(head)と、のこりの要素のリスト(tail)への参照を持つ。リストの終わりは空のリストで表現する
  * たとえば、要素が 'wan' しかない `SinglyLinkedList` の head は 'wan' を指し、tail は空のリストを指す。
  * 要素に 'wan', 'nyan' を持つ `SinglyLinkedList` の head は 'wan' を指し、tail は 'nyan' のみを持つリストを指す
* `SinglyLinkedList::Null.new` は引数を取らず、`SinglyLinkedList.new` は `head` と `tail` を引数に取る。つまり、
  * `SinglyLinkedList::Null.new` で、空のリストが返る
  * `SinglyLinkedList.new('nyan', SinglyLinkedList::Null.new)` で、'nyan'を要素に持つリストが返る
  * `SinglyLinkedList.new('wan', SinglyLinkedList.new('nyan', SinglyLinkedList::Null.new))` で、'wan', 'nyan'を要素に持つリストが返る
* `SinglyLinkedList#head` で head を取り出せる
* `SinglyLinkedList#tail` で tail を取り出せる
* `SinglyLinkedList#size` で そのリストに格納されている要素の数が返る
* `SinglyLinkedList#to_a` で,そのリストの内容を配列に変換したものが返る
* `==` で SinglyLinkedList 同士を比べた場合、要素がすべて同値ならば true となる
* `SinglyLinkedList[1, 2, 3]` のようにすると、要素に1, 2, 3を持ったSinglyLinkedList が得られる

これらの要件をテストに落とし込んだものが `spec/singly_linked_list_spec.rb` に書いてあるので、このテストがグリーンになるような`lib/singly_linked_list.rb` を実装する。
