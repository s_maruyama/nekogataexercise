root = File.dirname(File.dirname(__FILE__))
$LOAD_PATH.unshift File.join(root, 'lib')
require "singly_linked_list"

describe SinglyLinkedList::Null do
  subject { SinglyLinkedList::Null.new }

  it { should be_a_kind_of(SinglyLinkedList) }
  its(:size) { should == 0 }
  its(:to_a) { should == [] }
end

describe SinglyLinkedList do
  describe ".new" do
    it "should raise ArgumentError if second argument is not a SinglyLinkedList" do
      expect {SinglyLinkedList.new(1,2)}.to raise_error(ArgumentError)
    end
  end

  describe "#==" do
    it "should return true when inner values are equivalent (not identical)" do
      expect(SinglyLinkedList.new('nyan', SinglyLinkedList.new('wan', SinglyLinkedList::Null.new))).to \
        eq(SinglyLinkedList.new('nyan', SinglyLinkedList.new('wan', SinglyLinkedList::Null.new)))
    end
    it "should return false when inner values are not equivalent" do
      expect(SinglyLinkedList.new('hoge', SinglyLinkedList.new('fuga', SinglyLinkedList::Null.new))).to_not \
        eq(SinglyLinkedList.new('nyan', SinglyLinkedList.new('wan', SinglyLinkedList::Null.new)))
    end
  end

  context "when it has a single element(['nyan'])" do
    let(:tail) { SinglyLinkedList::Null.new }
    subject { SinglyLinkedList.new('nyan', tail) }

    its(:size) { should == 1 }
    its(:head) { should == 'nyan' }
    its(:tail) { should == tail }
    its(:to_a) { should == ['nyan'] }
  end

  context "when it has some elements( ['wan', 'nyan'])" do
    let (:tail) { SinglyLinkedList.new('nyan', SinglyLinkedList::Null.new) }
    subject { SinglyLinkedList.new('wan', tail) }

    its(:size) { should == 2 }
    its(:head) { should == 'wan' }
    its(:tail) { should == tail }
    its(:to_a) { should == ['wan', 'nyan'] }
  end

  describe ".[]" do
    context "when given 1, 2, 3" do
      subject { SinglyLinkedList[1,2,3] }
      it "should equivalant to SinglyLinkedList which has elements 1, 2, 3" do
        should == SinglyLinkedList.new(1, SinglyLinkedList.new(2, SinglyLinkedList.new(3, SinglyLinkedList::Null.new)))
      end
    end
  end
end

