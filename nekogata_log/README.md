# 猫型ログパーサーの実装

手元に以下のようなログファイルが存在する。

```
2013/01/12	65.2	21.0	26.1
2013/01/13	66.4	22	22.3
2013/01/14	64.1	21.1	25.0
2013/01/15	68.5	24.5	23.1
2013/01/16	69.1	25.1	26.5
```

これは、猫型氏の身体情報の書かれたログであり、タブ区切りで1列目から「日にち」「体重」「体脂肪率」「BMI」である。

これらをパースする、以下のクラスを作成せよ。また、それらを rspec でテストする spec ファイルも作成せよ。

## NekogataLog
ログをパースするインターフェイスを提供するクラス

* `NekogataLog.new(file)` は、open済みの File オブジェクトを受け取る
* `NekogataLog.open(file_path)` は、ファイルパスを受け取る
* `NekogataLog.from_string(string)` は、ログファイルの中身の文字列を受け取る
* `NekogataLog` は、`NekogataLog::Row`を要素とする`Enumurable`として振る舞う(言い換えれば、`NekogataLog::Row`を引数にとるブロックを受け取る`#each`を定義し、`Enumurable`を `include` する)


## NekogataLog::Row

ログの一行を表すクラス。

* `NekogataLog::Row#date` で、その行に書かれている日にちをDate型のオブジェクトで返す
* `nekogataLog::Row#weight` で、その行に書かれている体重をFloatで返す
* `nekogataLog::Row#body_fat_percentage` で、その行に書かれている体脂肪率をFloatで返す
* `nekogataLog::Row#bmi` で、その行に書かれているBMIをFloatで返する
